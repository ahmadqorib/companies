<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Companies;
use App\Repository\EmployeeRepository;

class EmployeeController extends Controller
{
    protected $emp;

    public function __construct(EmployeeRepository $emp)
    {
        $this->emp = $emp;
    }

    public function index()
    {
        return view('employee.index')
            ->withTitle('Data Karyawan')
            ->withData($this->emp->getPaginate(5));
    }

    public function create()
    {
        $company = new Companies();
        return view('employee.create')
            ->withData(new Employee)
            ->withCompany($company->all())
            ->withTitle('Tambah Data Karyawan');
    }

    public function store(Request $request)
    {
        $this->emp->validation($request);
        
        $data = [
            'nama' => $request->nama,
            'email' => $request->email,
            'company' => $request->company,
        ];
        $save = $this->emp->saveUpdate($data);

        if($save == true){
            return redirect()->route('employee.index')->with('success', 'Data berhasil disimpan');
        }else{
            return redirect()->route('employee.index')->with('fail', 'Data gagal disimpan');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $company = new Companies();
        return view('employee.edit')
            ->withTitle('Edit Data Karyawan')
            ->withData($this->emp->getById($id))
            ->withCompany($company->all());
    }

    public function update(Request $request, $id)
    {
        $this->emp->validation($request);
        
        $data = [
            'nama' => $request->nama,
            'email' => $request->email,
            'company' => $request->company,
        ];
        $save = $this->emp->saveUpdate($data, $id);

        if($save == true){
            return redirect()->route('employee.index')->with('success', 'Data berhasil diperbaharui');
        }else{
            return redirect()->route('employee.index')->with('fail', 'Data gagal diperbaharui');
        }
    }

    public function destroy($id)
    {
        $delete = Employee::where('id', $id)->delete();
        if($delete){
            return redirect()->route('employee.index')->with('success', 'Data berhasil dihapus');
        }else{
            return redirect()->route('employee.index')->with('fail', 'Data gagal dihapus');
        }
    }
}
