<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;
use App\Repository\CompaniesRepository;
use Illuminate\Support\Facades\Storage;

class CompaniesController extends Controller
{
    protected $comp;

    public function __construct(CompaniesRepository $comp)
    {
        $this->comp = $comp;
    }

    public function index()
    {
        return view('company.index')
            ->withTitle('Data Perusahaan')
            ->withData($this->comp->getPaginate(5));
    }

    public function create()
    {
        return view('company.create')
            ->withData(new Companies)
            ->withTitle('Tambah Data Perusahaan');
    }

    public function store(Request $request)
    {
        $this->comp->validation($request);
        
        $upload = $this->comp->uploadImage($request);
        if($upload != false){
            $data = [
                'nama' => $request->nama,
                'email' => $request->email,
                'website' => $request->website,
                'logo' => $upload
            ];
            $save = $this->comp->saveUpdate($data);
        }

        if($save == true){
            return redirect()->route('companies.index')->with('success', 'Data berhasil disimpan');
        }else{
            return redirect()->route('companies.index')->with('fail', 'Data gagal disimpan');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('company.edit')
            ->withTitle('Edit Data Perusahaan')
            ->withData($this->comp->getById($id));
    }

    public function update(Request $request, $id)
    {

        $data = [
            'nama' => $request->nama,
            'email' => $request->email,
            'website' => $request->website
        ];

        $this->comp->validation($request, $request->check);
        
        if($request->hasFile('logo')) {
            $upload = $this->comp->uploadImage($request);
            $data = [
                'nama' => $request->nama,
                'email' => $request->email,
                'website' => $request->website,
                'logo' => $upload
            ];
        }

        $update = $this->comp->saveUpdate($data, $id);
        if($update == true){
            return redirect()->route('companies.index')->with('success', 'Data berhasil diupdate');
        }else{
            return redirect()->route('companies.index')->with('fail', 'Data gagal diupdate');
        }

    }

    public function destroy($id)
    {
        $delete = Companies::where('id', $id)->delete();
        if($delete){
            return redirect()->route('companies.index')->with('success', 'Data berhasil dihapus');
        }else{
            return redirect()->route('companies.index')->with('fail', 'Data gagal dihapus');
        }
    }
}
