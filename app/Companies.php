<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\URL;

class Companies extends Model
{
    protected $table = 'companies';
    protected $fillable = ['nama', 'email', 'website', 'logo'];

    public function getLogoAttribute($value)
    {
        return asset('storage/company/'.$value);
    }
}
