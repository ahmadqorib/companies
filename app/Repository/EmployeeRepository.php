<?php

namespace App\Repository;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class EmployeeRepository
{
    protected $emp;

    public function __construct(Employee $emp)
    {
        $this->emp = $emp;
    }

    public function getAll()
    {
        return $this->emp->all();
    }

    public function getById($id)
    {
        return $this->emp->find($id);
    }

    public function getPaginate($page)
    {
        return $this->emp->orderBy('nama', 'ASC')->paginate($page);
    }

    public function validation($request)
    {   
        $data = [
            'nama' => 'required',
            'email' => 'required|email',
            'company' => 'required',
        ];
        return $validatedData = $request->validate($data);
    }

    public function saveUpdate($data, $id = null)
    {
        if($id == null){
            return $save = $this->emp->create($data);
        }
        return $update = $this->emp->where('id', $id)->update($data);
        
    }
}
