<?php

namespace App\Repository;

use Illuminate\Http\Request;
use App\Companies;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CompaniesRepository
{
    protected $comp;

    public function __construct(Companies $comp)
    {
        $this->comp = $comp;
    }

    public function getAll()
    {
        return $this->comp->all();
    }

    public function getPaginate($page)
    {
        return $this->comp->orderBy('nama', 'ASC')->paginate($page);
    }

    public function getById($id)
    {
        return $this->comp->find($id);
    }

    public function validation($request, $check = null)
    {   
        $data = [
            'nama' => 'required',
            'email' => 'required|email',
            'website' => 'required',
            'logo' => 'required|dimensions:min_width=100,min_height=100|max:2000|mimes:png',
        ];

        if($check != null){
            $data = [
                'nama' => 'required',
                'email' => 'required|email',
                'website' => 'required',
                'logo' => 'dimensions:min_width=100,min_height=100|max:2000|mimes:png',
            ];
        }
        return $validatedData = $request->validate($data);
    }

    public function saveUpdate($data, $id = null)
    {
        if($id == null){
            return $save = $this->comp->create($data);
        }
        return $update = $this->comp->where('id', $id)->update($data);
        
    }

    public function uploadImage($request){
        $file = $request->file('logo');
        $name = time();
        $extensi = $file->getClientOriginalExtension();
        $save_name = $name.'.'.$extensi;

        $upload = Storage::putFileAs('public/company', $file, $save_name);
        if($upload){
            return $save_name;
        }else{
            return false;
        }
        
    }
}
