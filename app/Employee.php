<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $fillable = ['nama', 'company', 'email'];

    public function companies()
    {
        return $this->belongsTo(Companies::class, 'company', 'id');
    }
}
