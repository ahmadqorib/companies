<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "PT Transisi Teknologi Mandiri",
            'email' => "admin@transisi.id",
            'email_verified_at' => now(),
            'password' => Hash::make("transisi"),
            'remember_token' => Str::random(10),
        ]);
    }
}
