<?php

use Illuminate\Database\Seeder;
use App\Companies;
use App\Employee;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $comp = new Companies();
        for($i = 1; $i<=20; $i++){
            $comp = new Companies();
            $comp->nama = "PT ".Str::random(10);
            $comp->email = Str::random(10).'@gmail.com';
            $comp->logo = "1569089478.png";
            $comp->website = Str::random(10).".com";
            $comp->save();

            $emp = new Employee();
            $emp->nama = Str::random(10);
            $emp->email = Str::random(10).'@gmail.com';
            $emp->company = $comp->id;
            $emp->save();
        }
    }
}
