@csrf
<div class="form-group">
    <label for="nama" class="text-md-right">{{ __('Nama Karyawan') }}</label>
    <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama', $data->nama) }}" autofocus>
    @error('nama')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="email" class="text-md-right">{{ __('Email') }}</label>
    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $data->email) }}">
    @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="company" class="text-md-right">{{ __('Perusahaan') }}</label>
    <select name="company" id="company" class="form-control @error('company') is-invalid @enderror">
        <option value="">Pilih Perusahaan</option>
        @foreach($company as $comp)
            <option value="{{ $comp->id }}" {{ old('company', $data->company) == $comp->id ? "selected":"" }}>{{ $comp->nama }}</option>
        @endforeach
    </select>
    @error('company')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group mb-0">
    <button type="submit" class="btn btn-primary">
        {{ $buttonText }}
    </button>
</div>