@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Data Karyawan
                        <a href="{{ route('employee.create') }}" class="btn btn-success btn-sm float-right">tambah</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No.</td>
                                    <td>Nama Karyawan</td>
                                    <td>Perusahaan</td>
                                    <td>Email</td>
                                    <td width="5%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $emp)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $emp->nama }}</td>
                                    <td>{{ $emp->companies['nama'] }}</td>
                                    <td>{{ $emp->email }}</td>
                                    <td>
                                        <a href="{{ route('employee.edit', $emp->id ) }}" class="btn btn-primary btn-sm">edit</a>
                                        <form method="POST" action="{{ route('employee.destroy', $emp->id) }}" class="my-1">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection