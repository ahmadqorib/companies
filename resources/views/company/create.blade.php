@extends('layouts.app')

@section('content')
    <div class="container py-3">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Tambah Data Perusahaan
                        <a href="{{ route('companies.index') }}" class="btn btn-danger btn-sm float-right">kembali</a>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('companies.store') }}" enctype="multipart/form-data">
                            @include ("company._form", ['buttonText' => "Simpan"])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection