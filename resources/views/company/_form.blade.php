@csrf
<div class="form-group">
    <label for="nama" class="text-md-right">{{ __('Nama Perusahaan') }}</label>
    <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama', $data->nama) }}" autofocus>
    @error('nama')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="email" class="text-md-right">{{ __('Email') }}</label>
    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $data->email) }}">
    @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="website" class="text-md-right">{{ __('Website') }}</label>
    <input id="website" type="text" class="form-control @error('website') is-invalid @enderror" name="website" value="{{ old('website', $data->website) }}">
    @error('website')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group">
    <label for="logo" class="text-md-right">{{ __('Logo Perusahaan') }}</label>
    <input id="logo" type="file" name="logo" class="form-control @error('logo') is-invalid @enderror">
    <input type="hidden" value="{{ $data->id }}" name="check">
    @error('logo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group mb-0">
    <button type="submit" class="btn btn-primary">
        {{ $buttonText }}
    </button>
</div>