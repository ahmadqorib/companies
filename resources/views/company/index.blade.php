@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Data Perusahaan
                        <a href="{{ route('companies.create') }}" class="btn btn-success btn-sm float-right">tambah</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>No.</td>
                                    <td>Logo</td>
                                    <td>Nama Perusahaan</td>
                                    <td>Email</td>
                                    <td>Website</td>
                                    <td width="5%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $comp)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        <img src="{{ $comp->logo }}" class="img-fluid img-thumbnail" width="120">
                                    </td>
                                    <td>{{ $comp->nama }}</td>
                                    <td>{{ $comp->email }}</td>
                                    <td><a href="http://{{ $comp->website }}" target="_blank">{{ $comp->website }}</a></td>
                                    <td>
                                        <a href="{{ route('companies.edit', $comp->id ) }}" class="btn btn-primary btn-sm">edit</a>
                                        <form method="POST" action="{{ route('companies.destroy', $comp->id) }}" class="my-1">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection